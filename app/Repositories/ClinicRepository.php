<?php

namespace App\Repositories;

use App\Models\Clinic;
use App\Repositories\BaseRepository;

/**
 * Class ClinicRepository
 * @package App\Repositories
 * @version June 17, 2020, 11:27 pm UTC
*/

class ClinicRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'schedule',
        'specialty',
        'celphone',
        'address'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clinic::class;
    }
}
