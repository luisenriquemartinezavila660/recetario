<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Recipe
 * @package App\Models
 * @version June 17, 2020, 11:41 pm UTC
 *
 * @property string $name
 * @property string $age
 * @property string $sex
 * @property string $date_recipe
 * @property string $diagnosis
 * @property string $indications
 * @property string $upcoming_appointments
 * @property string $doctor_id
 */
class Recipe extends Model
{
    use SoftDeletes;

    public $table = 'recipes';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'age',
        'sex',
        'date_recipe',
        'diagnosis',
        'indications',
        'upcoming_appointments',
        'doctor_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'age' => 'string',
        'sex' => 'string',
        'date_recipe' => 'date',
        'diagnosis' => 'string',
        'indications' => 'longText',
        'upcoming_appointments' => 'string',
        'doctor_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'age' => 'required',
        'sex' => 'required',
        'date_recipe' => 'required',
        'diagnosis' => 'required',
        'indications' => 'required',
        'doctor_id' => 'required'
    ];

    public function doctors(){
        return $this->hasOne('App\Models\Doctor');
    }


}
