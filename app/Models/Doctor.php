<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Doctor
 * @package App\Models
 * @version June 17, 2020, 11:13 pm UTC
 *
 * @property string $name
 * @property string $celphone
 * @property string $cp
 */
class Doctor extends Model
{
    use SoftDeletes;

    public $table = 'doctors';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'celphone',
        'cp'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'celphone' => 'string',
        'cp' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'celphone' => 'required',
        'cp' => 'required'
    ];

    public function recipers(){
        return $this->belongsTo('App\Models\Recipe');
    }


}
