<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Clinic
 * @package App\Models
 * @version June 17, 2020, 11:27 pm UTC
 *
 * @property string $name
 * @property string $schedule
 * @property string $specialty
 * @property string $celphone
 * @property string $address
 */
class Clinic extends Model
{
    use SoftDeletes;

    public $table = 'clinics';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'schedule',
        'specialty',
        'celphone',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'schedule' => 'string',
        'specialty' => 'string',
        'celphone' => 'string',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'schedule' => 'required',
        'specialty' => 'required',
        'celphone' => 'required',
        'address' => 'required'
    ];

    
}
