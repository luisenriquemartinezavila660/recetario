<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClinicRequest;
use App\Http\Requests\UpdateClinicRequest;
use App\Repositories\ClinicRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ClinicController extends AppBaseController
{
    /** @var  ClinicRepository */
    private $clinicRepository;

    public function __construct(ClinicRepository $clinicRepo)
    {
        $this->clinicRepository = $clinicRepo;
    }

    /**
     * Display a listing of the Clinic.
     *
     * @param Request $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Request $request)
    {
        $clinics = $this->clinicRepository->all();

        return view('clinics.index')
            ->with('clinics', $clinics);
    }

    /**
     * Show the form for creating a new Clinic.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Response
     */
    public function create()
    {
        return view('clinics.create');
    }

    /**
     * Store a newly created Clinic in storage.
     *
     * @param CreateClinicRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function store(CreateClinicRequest $request)
    {
        $input = $request->all();

        $clinic = $this->clinicRepository->create($input);

        Flash::success('Clinic saved successfully.');

        return redirect(route('clinicas.index'));
    }

    /**
     * Display the specified Clinic.
     *
     * @param int $id
     *
     * @return Factory|RedirectResponse|Redirector|View|Response
     */
    public function show($id)
    {
        $clinic = $this->clinicRepository->find($id);

        if (empty($clinic)) {
            Flash::error('Clinic not found');

            return redirect(route('clinicas.index'));
        }

        return view('clinics.show')->with('clinic', $clinic);
    }

    /**
     * Show the form for editing the specified Clinic.
     *
     * @param int $id
     *
     * @return Factory|RedirectResponse|Redirector|View|Response
     */
    public function edit($id)
    {
        $clinic = $this->clinicRepository->find($id);

        if (empty($clinic)) {
            Flash::error('Clinic not found');

            return redirect(route('clinicas.index'));
        }

        return view('clinics.edit')->with('clinic', $clinic);
    }

    /**
     * Update the specified Clinic in storage.
     *
     * @param int $id
     * @param UpdateClinicRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function update($id, UpdateClinicRequest $request)
    {
        $clinic = $this->clinicRepository->find($id);

        if (empty($clinic)) {
            Flash::error('Clinic not found');

            return redirect(route('clinicas.index'));
        }

        $clinic = $this->clinicRepository->update($request->all(), $id);

        Flash::success('Clinic updated successfully.');

        return redirect(route('clinicas.index'));
    }

    /**
     * Remove the specified Clinic from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function destroy($id)
    {
        $clinic = $this->clinicRepository->find($id);

        if (empty($clinic)) {
            Flash::error('Clinic not found');

            return redirect(route('clinicas.index'));
        }

        $this->clinicRepository->delete($id);

        Flash::success('Clinic deleted successfully.');

        return redirect(route('clinicas.index'));
    }
}
