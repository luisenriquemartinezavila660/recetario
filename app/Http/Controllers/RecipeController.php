<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecipeRequest;
use App\Http\Requests\UpdateRecipeRequest;
use App\Repositories\RecipeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use App\Models\Doctor;
use App\Models\Clinic;
use Response;

class RecipeController extends AppBaseController
{
    /** @var  RecipeRepository */
    private $recipeRepository;

    public function __construct(RecipeRepository $recipeRepo)
    {
        $this->recipeRepository = $recipeRepo;
    }

    /**
     * Display a listing of the Recipe.
     *
     * @param Request $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Request $request)
    {
        $doctor= Doctor::pluck('name','id');

        $recipes = $this->recipeRepository->all();

        return view('recipes.index')
            ->with('recipes', $recipes)
            ->with('doctor',$doctor);
    }

    /**
     * Show the form for creating a new Recipe.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Response
     */
    public function create()
    {
        $doctor= Doctor::pluck('name','id');
        //dd($doctor);
        return view('recipes.create')->with('doctor',$doctor);
    }

    /**
     * Store a newly created Recipe in storage.
     *
     * @param CreateRecipeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function store(CreateRecipeRequest $request)
    {
        $input = $request->all();
        $doctor= Doctor::find(1);
        $clinic = Clinic::find(1);
        $recipe = $this->recipeRepository->create($input);
        $pdf = \PDF::loadView('pdf',['input'=>$input,'doctor'=>$doctor]);
        $pdf->download('primerpdf.pdf');

        Flash::success('Recipe saved successfully.');
        return redirect(route('recetas.index'));
        //return $pdf->download('primerpdf.pdf');

    }

    /**
     * Display the specified Recipe.
     *
     * @param int $id
     *
     * @return Factory|RedirectResponse|Redirector|View|Response
     */
    public function show($id)
    {
        $recipe = $this->recipeRepository->find($id);

        if (empty($recipe)) {
            Flash::error('Recipe not found');

            return redirect(route('recetas.index'));
        }

        return view('recipes.show')->with('recipe', $recipe);
    }

    /**
     * Show the form for editing the specified Recipe.
     *
     * @param int $id
     *
     * @return Factory|RedirectResponse|Redirector|View|Response
     */
    public function edit($id)
    {
        $recipe = $this->recipeRepository->find($id);

        if (empty($recipe)) {
            Flash::error('Recipe not found');

            return redirect(route('recetas.index'));
        }

        return view('recipes.edit')->with('recipe', $recipe);
    }

    /**
     * Update the specified Recipe in storage.
     *
     * @param int $id
     * @param UpdateRecipeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function update($id, UpdateRecipeRequest $request)
    {
        $recipe = $this->recipeRepository->find($id);

        if (empty($recipe)) {
            Flash::error('Recipe not found');

            return redirect(route('recetas.index'));
        }

        $recipe = $this->recipeRepository->update($request->all(), $id);

        Flash::success('Recipe updated successfully.');

        return redirect(route('recetas.index'));
    }

    /**
     * Remove the specified Recipe from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     */
    public function destroy($id)
    {
        $recipe = $this->recipeRepository->find($id);

        if (empty($recipe)) {
            Flash::error('Recipe not found');

            return redirect(route('recetas.index'));
        }

        $this->recipeRepository->delete($id);

        Flash::success('Recipe deleted successfully.');

        return redirect(route('recetas.index'));
    }
}
