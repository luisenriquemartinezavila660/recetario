<div class="table-responsive">
    <table class="table" id="recipes-table">
        <thead>
            <tr>
                <th>Paciente</th>
        <th>Edad</th>
        <th>Sexo</th>
        <th>Fecha</th>
        <th>Diagnostico</th>
                <th colspan="3">Administrar</th>
            </tr>
        </thead>
        <tbody>
        @foreach($recipes as $recipe)
            <tr>
                <td>{{ $recipe->name }}</td>
            <td>{{ $recipe->age }}</td>
            <td>{{ $recipe->sex }}</td>
            <td>{{ $recipe->date_recipe }}</td>
            <td>{{ $recipe->diagnosis }}</td>
                <td>
                    {!! Form::open(['route' => ['recetas.destroy', $recipe->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('recetas.show', [$recipe->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('recetas.edit', [$recipe->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Desea eliminar la receta de: $recipe->name')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
