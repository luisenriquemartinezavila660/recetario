<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre del Paciente:') !!}
    <p>{{ $recipe->name }}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Edad:') !!}
    <p>{{ $recipe->age }}</p>
</div>

<!-- Sex Field -->
<div class="form-group">
    {!! Form::label('sex', 'Sexo:') !!}
    <p>{{ $recipe->sex }}</p>
</div>

<!-- Date Recipe Field -->
<div class="form-group">
    {!! Form::label('date_recipe', 'Fecha:') !!}
    <p>{{ $recipe->date_recipe }}</p>
</div>

<!-- Diagnosis Field -->
<div class="form-group">
    {!! Form::label('diagnosis', 'Diagnostico:') !!}
    <p>{{ $recipe->diagnosis }}</p>
</div>

<!-- Indications Field -->
<div class="form-group">
    {!! Form::label('indications', 'Indicaciones:') !!}
    <p>{{ $recipe->indications }}</p>
</div>

<!-- Upcoming Appointments Field -->
<div class="form-group">
    {!! Form::label('upcoming_appointments', 'Proxima cita:') !!}
    <p>{{ $recipe->upcoming_appointments }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', 'Doctor:') !!}
    <p>{{ $recipe->doctor_id }}</p>
</div>


