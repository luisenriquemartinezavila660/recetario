<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nombre del Paciente:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-3">
    {!! Form::label('age', 'Edad:') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Sex Field -->
<div class="form-group col-sm-3">
    {!! Form::label('sex', 'Sexo:') !!}
    <select name="sex" class="form-control">
        <option value="MASCULINO">Masculino</option>
        <option value="FEMENINO">Femenino</option>
      </select>
</div>

<!-- Date Recipe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_recipe', 'Fecha:') !!}
    {!! Form::date('date_recipe', null, ['class' => 'form-control','id'=>'date_recipe']) !!}
</div>



<!-- Diagnosis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('diagnosis', 'Diagnostico:') !!}
    {!! Form::text('diagnosis', null, ['class' => 'form-control']) !!}
</div>

<!-- Indications Field -->


<!-- Upcoming Appointments Field -->


<!-- Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doctor_id', 'Doctor:') !!}
    {!! Form::select('doctor_id', $doctor, ['class' => 'form-control']) !!}

</div>

    <div class="form-group col-md-12">
        {!! Form::label('indications', 'Indicaciones:') !!}
        {!! Form::textarea('indications', null, ['class' => 'form-control']) !!}
    </div>


    <div class="form-group col-sm-6">
        {!! Form::label('upcoming_appointments', 'Proxima Cita:') !!}
        {!! Form::text('upcoming_appointments', null, ['class' => 'form-control']) !!}
    </div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <button  class="btn btn-primary" href="{{route('imprimir')}}" formtarget="_blank">Generar PDF</button>

    <a href="{{ route('recetas.index') }}" class="btn btn-default">Cancelar</a>
</div>
