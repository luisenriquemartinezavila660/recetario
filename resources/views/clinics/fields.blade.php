<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre de la Clinica:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Schedule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('schedule', 'Horario:') !!}
    {!! Form::text('schedule', null, ['class' => 'form-control']) !!}
</div>

<!-- Specialty Field -->


<!-- Celphone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celphone', 'Telefono:') !!}
    {!! Form::text('celphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Direccion:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('specialty', 'Especialidades:') !!}
    {!! Form::textarea('specialty', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clinicas.index') }}" class="btn btn-default">Cancelar</a>
</div>
