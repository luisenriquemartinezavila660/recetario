<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre de la Clinica:') !!}
    <p>{{ $clinic->name }}</p>
</div>

<!-- Schedule Field -->
<div class="form-group">
    {!! Form::label('schedule', 'Horario:') !!}
    <p>{{ $clinic->schedule }}</p>
</div>

<!-- Specialty Field -->
<div class="form-group">
    {!! Form::label('specialty', 'Especialidades:') !!}
    <p>{{ $clinic->specialty }}</p>
</div>

<!-- Celphone Field -->
<div class="form-group">
    {!! Form::label('celphone', 'Telefono:') !!}
    <p>{{ $clinic->celphone }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Direccion:') !!}
    <p>{{ $clinic->address }}</p>
</div>
