<div class="table-responsive">
    <table class="table" id="clinics-table">
        <thead>
            <tr>
                <th>Nombre de la Clinica</th>
        <th>Horario</th>
        <th>Direccion</th>
                <th colspan="3">Administrar</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clinics as $clinic)
            <tr>
                <td>{{ $clinic->name }}</td>
            <td>{{ $clinic->schedule }}</td>
            <td>{{ $clinic->address }}</td>
                <td>
                    {!! Form::open(['route' => ['clinicas.destroy', $clinic->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('clinicas.show', [$clinic->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('clinicas.edit', [$clinic->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Desea eliminar la clinica: $clinic->name')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
