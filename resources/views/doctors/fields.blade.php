<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre del Doctor:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Celphone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celphone', 'Celular:') !!}
    {!! Form::text('celphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cedula Profecional:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctores.index') }}" class="btn btn-default">Cancelar</a>
</div>
