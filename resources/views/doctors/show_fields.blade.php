<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre del Doctor:') !!}
    <p>{{ $doctor->name }}</p>
</div>

<!-- Celphone Field -->
<div class="form-group">
    {!! Form::label('celphone', 'Celular:') !!}
    <p>{{ $doctor->celphone }}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cedula Profecional:') !!}
    <p>{{ $doctor->cp }}</p>
</div>


