<li class="{{ Request::is('doctors*') ? 'active' : '' }}">
    <a href="{{ route('doctores.index') }}"><i class="fa fa-edit"></i><span>Doctores</span></a>
</li>

<li class="{{ Request::is('clinics*') ? 'active' : '' }}">
    <a href="{{ route('clinicas.index') }}"><i class="fa fa-edit"></i><span>Clinica</span></a>
</li>

<li class="{{ Request::is('recipes*') ? 'active' : '' }}">
    <a href="{{ route('recetas.index') }}"><i class="fa fa-edit"></i><span>Recetas</span></a>
</li>

