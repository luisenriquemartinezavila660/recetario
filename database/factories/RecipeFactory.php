<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Recipe;
use Faker\Generator as Faker;

$factory->define(Recipe::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'age' => $faker->word,
        'sex' => $faker->word,
        'date_recipe' => $faker->word,
        'diagnosis' => $faker->word,
        'indications' => $faker->word,
        'upcoming_appointments' => $faker->word,
        'doctor_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
