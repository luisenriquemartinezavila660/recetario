<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Clinic;
use Faker\Generator as Faker;

$factory->define(Clinic::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'schedule' => $faker->word,
        'specialty' => $faker->word,
        'celphone' => $faker->word,
        'address' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
